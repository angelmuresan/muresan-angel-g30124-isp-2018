package g30124.muresan.angel.l3.e3;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;

public class Ex3 {

	  public static void main(String[] args)
      {
          City cluj = new City();

          Robot bobo = new Robot(cluj, 5, 1, Direction.NORTH);

          // Direct the robot to the final situation
          bobo.move();
          bobo.move();
          bobo.move();
          bobo.move();
          bobo.move();
          bobo.turnLeft();	// start turning right as three turn lefts
          bobo.turnLeft();
          bobo.move();
          bobo.move();
          bobo.move();
          bobo.move();
          bobo.move();

          bobo.move();
      }
	  
}
