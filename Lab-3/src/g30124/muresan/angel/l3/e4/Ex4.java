package g30124.muresan.angel.l3.e4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Ex4 {

	public static void main (String[]args)
    {
        
        City cluj = new City();
        Wall blockAve0 = new Wall(cluj, 1, 1, Direction.WEST);
        Wall blockAve1 = new Wall(cluj, 2, 1, Direction.WEST);
        Wall blockAve2 = new Wall(cluj, 1, 1, Direction.NORTH);
        Wall blockAve3 = new Wall(cluj, 1, 2, Direction.NORTH);
        Wall blockAve4 = new Wall(cluj, 1, 3, Direction.WEST);
        Wall blockAve5 = new Wall(cluj, 2, 3, Direction.WEST);
        Wall blockAve6 = new Wall(cluj, 2, 1, Direction.SOUTH);
        Wall blockAve7 = new Wall(cluj, 2, 2, Direction.SOUTH);
        Robot bobo = new Robot(cluj, 0, 2, Direction.WEST);

        bobo.move();
        bobo.move();
        bobo.turnLeft();
        bobo.move();
        bobo.move();
        bobo.move();
        bobo.turnLeft();
        bobo.move();
        bobo.move();
        bobo.move();
        bobo.turnLeft();
        bobo.move();
        bobo.move();
        bobo.move();
        bobo.turnLeft();    
        bobo.move();

    }
	
}
