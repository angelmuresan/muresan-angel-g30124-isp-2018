package g30124.muresan.angel.l3.e5;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;
import becker.robots.*;

public class Ex5 {

	public static void main(String[] args)
    {
        // Set up the initial situation
        City cluj = new City();
        Wall blockAve0 = new Wall(cluj, 1, 1, Direction.WEST);
        Wall blockAve1 = new Wall(cluj, 2, 1, Direction.WEST);
        Wall blockAve3 = new Wall(cluj, 1, 1, Direction.NORTH);
        Wall blockAve4 = new Wall(cluj, 1, 2, Direction.NORTH);
        Wall blockAve6 = new Wall(cluj, 1, 2, Direction.EAST);
        Wall blockAve9 = new Wall(cluj, 1, 2, Direction.SOUTH);
        Wall blockAve10 = new Wall(cluj, 2, 1, Direction.SOUTH);
        
        Thing parcel = new Thing(cluj, 2, 2);
        Robot bobo = new Robot(cluj, 1, 2, Direction.SOUTH);

        bobo.turnLeft();	
        bobo.turnLeft();
        bobo.turnLeft();
        bobo.move();
        bobo.turnLeft();
        bobo.move();

        bobo.turnLeft();

        bobo.move();
        bobo.pickThing();
        bobo.turnLeft();
        bobo.turnLeft();
        bobo.move();
        bobo.turnLeft();
        bobo.turnLeft();
        bobo.turnLeft();
        bobo.move();
        bobo.turnLeft();
        bobo.turnLeft();
        bobo.turnLeft();
        bobo.move();
        bobo.turnLeft();
        bobo.turnLeft();
        bobo.turnLeft();
    }
	
}
