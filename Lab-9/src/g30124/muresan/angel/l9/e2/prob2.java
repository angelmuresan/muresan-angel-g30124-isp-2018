package g30124.muresan.angel.l9.e2;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;


public class prob2 implements ActionListener
{

    JButton Buton = new JButton("Butonas");
    JFrame o = new JFrame();  
    JTextField Afisare = new JTextField(15);

    int i = 0;

    public prob2(){

    	Afisare.setText("Numar :"+i);
        o.setTitle("Numarator");
        
        o.setVisible(true);
        o.setSize(200,100);
        
        o.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        o.setResizable(true);
        
        o.setLayout(new FlowLayout());
        
        o.add(Afisare);
        o.add(Buton);
        
        Buton.addActionListener(this);
        
    }


    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == Buton)
        {
            i++;
            Afisare.setText("Numar: "+i);
        }

    }
    public static void main(String args[]){
        new prob2();
    }


}