package g30124.muresan.angel.l6.e4;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestCharSeq {
    @Test
    public void testToString() {
        CharSeq c1 = new CharSeq("Incercare".toCharArray());
        assertEquals(c1.toString(),"Incercare".toCharArray().toString());
    }

}