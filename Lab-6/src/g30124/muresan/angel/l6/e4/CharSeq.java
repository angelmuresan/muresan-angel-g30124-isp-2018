package g30124.muresan.angel.l6.e4;

import java.util.Arrays;

class CharSeq implements CharSequence{

    private char[] chars;

    public CharSeq(char[] chars) {

        this.chars = chars;
    }

    @Override
    public int length() {

        return chars.length;
    }

    @Override
    public char charAt(int index) {
        return chars[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        char[] newc = Arrays.copyOfRange(chars,start,end);
        return new CharSeq(newc);
    }
}