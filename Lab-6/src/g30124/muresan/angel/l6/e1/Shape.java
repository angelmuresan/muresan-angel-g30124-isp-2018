package g30124.muresan.angel.l6.e1;

import java.awt.*;

public abstract class Shape {

    private int x,y;
    private Color color;
    private boolean fill;
    private String id;

    public Shape(int x,int y,Color color,boolean fill,String id) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.fill = fill;
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return color;
    }

    public boolean isFilled() {
        return fill;
    }

    public String getId() {
        return id;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}
