package g30124.muresan.angel.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(int x, int y, Color color, boolean fill, String id, int radius) {
        super(x,y,color,fill,id);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing circle "+getId()+" "+radius+" @ "+getX()+" "+getY()+" "+getColor().toString()+"filled: "+isFilled());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
            if(isFilled()){
                g.fillOval(getX(),getY(),radius,radius);
            }
    }
}
