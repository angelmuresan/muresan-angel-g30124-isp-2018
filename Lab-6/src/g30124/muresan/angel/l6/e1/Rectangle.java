package g30124.muresan.angel.l6.e1;

import java.awt.*;

public class Rectangle extends Shape {

    private int l;
    private int L;

    public Rectangle(int x, int y, Color color, boolean fill, String id, int L, int l) {
        super(x, y, color, fill, id);
        this.L = L;
        this.l = l;
    }

    public int getL() {

        return L;
    }

    public int getl() {
        return l;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing rectangle " + getId() + " " + L + " " + l + " @ " + getX() + " " + getY() + " " + getColor().toString() + " filled: " + isFilled());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), L, l);
            if (isFilled()) {
                g.fillRect(getX(),getY(),L,l);
        }
    }
}