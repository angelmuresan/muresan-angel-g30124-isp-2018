package g30124.muresan.angel.l6.e3;

import java.awt.Graphics;

interface Shape{
    void draw(Graphics g);
    String getId();
}