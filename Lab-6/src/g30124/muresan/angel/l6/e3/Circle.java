package g30124.muresan.angel.l6.e3;

import java.awt.*;

class Circle implements Shape {
    private int x,y;
    private Color color;
    private boolean fill;
    private String id;
    private int radius;

Circle(int x, int y, Color color, boolean fill, String id, int radius){
    this.x = x;
    this.y = y;
    this.color = color;
    this.fill = fill;
    this.id = id;
    this.radius = radius;
}

public String getId(){
    return id;
}

@Override
public void draw(Graphics g) {
    System.out.println("Drawing circle "+id+" "+radius+" @ "+x+" "+y+" "+color.toString()+"filled: "+fill);
    g.setColor(color);
    g.drawOval(x,y,radius,radius);
        if(fill){
            g.fillOval(x,y,radius,radius);
        }
}
}
