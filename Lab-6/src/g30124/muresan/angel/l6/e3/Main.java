package g30124.muresan.angel.l6.e3;

import java.awt.*;

/**
 * @author mihai.hulea
 */

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(50,50,Color.RED,false,"C1",50);
        b1.addShape(s1);
        Shape s2 = new Circle(60,50,Color.GREEN,true,"C2",50);
        b1.addShape(s2);
        Shape s3 = new Rectangle(100,150,Color.GREEN,true,"R",150,100);
        b1.addShape(s3);
        b1.deleteById("C2");
    }
}
