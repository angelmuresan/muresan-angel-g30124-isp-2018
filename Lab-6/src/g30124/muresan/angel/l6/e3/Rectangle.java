package g30124.muresan.angel.l6.e3;

import java.awt.*;

class Rectangle implements Shape {
    private int x,y;
    private Color color;
    private boolean fill;
    private String id;
    private int l;
    private int L;

Rectangle(int x, int y, Color color, boolean fill, String id, int L, int l) {
    this.x = x;
    this.y = y;
    this.color = color;
    this.fill = fill;
    this.id = id;
    this.L = L;
    this.l = l;
}

public String getId(){
   return id;
}

@Override
public void draw(Graphics g) {
    System.out.println("Drawing rectangle " +this.id+ " " + this.L + " " +this.l+ " @ " +this.x+ " " +this.y+ " " + this.color.toString() + " filled: " + this.fill);
    g.setColor(color);
    g.drawRect(x,y,L,l);
        if (this.fill) {
            g.fillRect(x,y,L,l);
    }
}
}