package g30124.muresan.angel.l7.e3;

import java.util.Iterator;
import java.util.TreeSet;

import g30124.muresan.angel.l7.e2.BankAccount;

public class BankWithTree{

    TreeSet<BankAccount> depozit = new TreeSet<BankAccount>();
    TreeSet<BankAccount> depozit1 = new TreeSet<BankAccount>(new SortByOwner());

    public void addAccount(String owner, double balance) {
        BankAccount cont = new BankAccount(owner, balance);
        depozit.add(cont);
        depozit1.add(cont);
    }



    public void printAccounts() {
        System.out.println("Depozitele sortate sunt: " );
        Iterator<BankAccount> iterator = depozit.iterator();
        while (iterator.hasNext()) {
            BankAccount account = iterator.next();
            System.out.println(account.getOwner() + " " + account.getBalance());
        }

    }

    public void printAccounts(int min, int max) {
        Iterator<BankAccount> it  = depozit.iterator();
        System.out.println("The bank depozit between " + min + " and " + max);
        while(it.hasNext()) {
            BankAccount account1 = it.next();
            if(min <= account1.getBalance() && max >= account1.getBalance()) {
                System.out.println(account1.getOwner() + " " + account1.getBalance());
            }
        }
    }

    public BankAccount getAccount(String owner){
        Iterator<BankAccount> it = depozit1.iterator();
        while(it.hasNext()) {
            BankAccount account2 = it.next();
            if(account2.getOwner().equals(owner)) {
                System.out.println(account2.getOwner() + " " + account2.getBalance());
                return account2;
            }
        }
        System.out.println("Contul "+ owner + " nu a fost gasit");
        return null;
    }

    public void getAllAcounts() {
        System.out.println("The depozit, sorted by owner, are:");
        Iterator<BankAccount> it = depozit1.iterator();
        while(it.hasNext()) {
            BankAccount account3 = it.next();
            System.out.println(account3.getOwner() + " " + account3.getBalance());
        }
    }
}