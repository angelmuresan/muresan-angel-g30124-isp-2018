package g30124.muresan.angel.l7.e3;

import java.util.Comparator;
import g30124.muresan.angel.l7.e2.BankAccount;

class SortByOwner implements Comparator<BankAccount> {
    @Override
    public int compare(BankAccount banca1, BankAccount banca2) {
        return banca1.getOwner().toUpperCase().compareTo(banca2.getOwner().toUpperCase());
    }
}
