package g30124.muresan.angel.l7.e3;

public class Main {
    public static void main(String[] args) {
        BankWithTree banca = new BankWithTree();
        banca.addAccount("Bianca", 10);
        banca.addAccount("Angel", 20);
        banca.addAccount("Bogdan", 30);
        banca.addAccount("Karina", 40);
        banca.printAccounts();
        banca.printAccounts(20, 40);
        banca.addAccount("Marco", 50);
        banca.addAccount("Tudor", 60);
        banca.printAccounts(20,50);
        banca.getAccount("Ildi");
        banca.getAllAcounts();

    }

}