package g30124.muresan.angel.l7.e2;

public class BankAccount {
    String owner;
    double balance;

    public void withdraw(double amount){
        balance=balance-amount;
    }

    public void deposit(double amount){
        balance=balance+amount;
    }

    public BankAccount(String owner, double balance){
        this.owner=owner;
        this.balance=balance;
    }

    public String getOwner(){
        return this.owner;
    }

    public double getBalance(){
        return this.balance;
    }

    public boolean equals(Object obiect) {
        if(obiect instanceof BankAccount){
            BankAccount banca = (BankAccount)obiect;
            return balance == banca.balance;
        }
        return false;
    }
}