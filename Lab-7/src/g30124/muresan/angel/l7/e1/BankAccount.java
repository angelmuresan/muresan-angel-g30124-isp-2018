package g30124.muresan.angel.l7.e1;

public class BankAccount {
    String owner;
    double balance;

    public void withdraw(double amount){
        balance=balance-amount;
    }

    public void deposit(double amount){
        balance=balance+amount;
    }

    BankAccount(String owner, double balance){
        this.owner=owner;
        this.balance=balance;
    }
    public boolean equals(Object obiect) {
        if(obiect instanceof BankAccount){
            BankAccount banca = (BankAccount)obiect;
            return balance == banca.balance;
        }
        return false;
    }
    public static void main(String[] args) {
        BankAccount banca1= new BankAccount("Bianca",3500);
        BankAccount banca2 = new BankAccount("Angel",3500);
        if(banca1.equals(banca2))

            System.out.println(banca1+" si "+banca2+ " sunt egale");
        else
            System.out.println(banca1+" si "+banca2+ " nu sunt egale");
    }
}
