package g30124.muresan.angel.l5.e1;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestShape {
    @Test
    public void testShape() {

        Shape[] forme=new Shape[3];
        forme[0]=new Circle("green",true,4.0);
        forme[1]=new Rectangle(1,2,"red",false);
        forme[2]=new Square(2,"blue",true);
    }
}