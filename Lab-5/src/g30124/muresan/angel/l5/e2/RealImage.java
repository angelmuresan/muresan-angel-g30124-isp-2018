package g30124.muresan.angel.l5.e2;

class RealImage implements Image {
    private String fileName;

    public RealImage(String fileName) {
        this.fileName=fileName;
    }
    @Override
    public void display() {
        System.out.println("Displaying " + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}
