package g30124.muresan.angel.l4.e7;

import g30124.muresan.angel.l4.e3.Circle;

public class Cylinder extends Circle {
	private double height;
	
	public Cylinder() {
		super();
	}
	
	public Cylinder(double radius) {
		super(radius);
	}
	
	public Cylinder(double radius, double height) {
		super(radius);
		this.height = height;
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getVolume() {
		return 3.14*(getRadius()*getRadius())*height;
	}
	
	public double getArea() {

	    return 2*3.14*getRadius()*(getRadius()*height);
	}
	
	public static void main(String[] args) {
		Cylinder c1 = new Cylinder(1,1);
		System.out.println(c1.getVolume());
		System.out.println(c1.getArea());
	}
}
