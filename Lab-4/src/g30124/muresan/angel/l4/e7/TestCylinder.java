package g30124.muresan.angel.l4.e7;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestCylinder {

	@Test
	public void testGetArea() {
		Cylinder c2 = new Cylinder(2,4);
		assertEquals(c2.getArea(),8,16);
	}

}