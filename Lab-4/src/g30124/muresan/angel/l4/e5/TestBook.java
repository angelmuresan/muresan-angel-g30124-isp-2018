package g30124.muresan.angel.l4.e5;

import g30124.muresan.angel.l4.e4.Author;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestBook {

	@Test
	public void testToString() {
		Author autor1 = new Author("IronMan","IronMan@email.com",'m');
		Book b = new Book("Poezii",autor1,23,45);
		assertEquals(b.toString(),"'Book: Poezii' by IronMan(m) at IronMan@email.com");
	}
}