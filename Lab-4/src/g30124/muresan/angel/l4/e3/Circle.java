package g30124.muresan.angel.l4.e3;

public class Circle {
    private double radius = 1.0;
    private String color = "Red";

    public Circle(){
        System.out.println(getRadius());
        System.out.println(getArea());
    }

    public Circle(double r){
        radius = r;
        System.out.println(getRadius());
        System.out.println(getArea());
    }

    public double getRadius(){
        return radius;
    }

    public double getArea(){
        return 3.14*radius*radius;
    }

    public static void main(String [] args){
        Circle cerc = new Circle();
        
    }
}