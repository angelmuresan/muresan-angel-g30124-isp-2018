package g30124.muresan.angel.l4.e4;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestAuthor {
	@Test
	public void testToString() {
		Author autor2 = new Author("IronMan", "IronMan@email.com",'m');
		assertEquals(autor2.toString(),"IronMan(m) at IronMan@email.com");
	}
}