package g30124.muresan.angel.l4.e4;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender){
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return this.name + "("+this.gender+") at " + this.email;
    }
    public static void main(String[] args) 
    {
		Author autor1 = new Author("Thor", "thor@ragnarok.com", 'm');
		System.out.println(autor1.toString());
	}
}