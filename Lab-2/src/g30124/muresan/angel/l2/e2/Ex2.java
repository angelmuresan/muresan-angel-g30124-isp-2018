package g30124.muresan.angel.l2.e2;

import java.util.Scanner;

public class Ex2 {
	public static void main(String[] args) {
        int nr;
        Scanner in = new Scanner(System.in);
		System.out.println("Introduceti a= "); 
		nr = in.nextInt();
		in.close();
		FunctieIf(nr);
		FunctiaSwitch(nr);
    }
    
    private static void FunctieIf(int nr)
    {
        String nrStr = null;
        if (0 == nr) {
            nrStr = "ZERO";
        } else if (1 == nr) {
            nrStr = "ONE";
        } else if (2 == nr) {
            nrStr = "TWO";
        } else if (3 == nr) {
            nrStr = "THREE";
        } else if (4 == nr) {
            nrStr = "FOUR";
        } else if (5 == nr) {
            nrStr = "FIVE";
        } else if (6 == nr) {
            nrStr = "SEX";
        } else if (7 == nr) {
            nrStr = "SEVEN";
        } else if (8 == nr) {
            nrStr = "EIGHT";
        } else if (9 == nr) {
            nrStr = "NINE";
        } else {
            nrStr = "OTHER";
        }
        System.out.println("(a) Folosind functia IF: " + nrStr);
    }
    
    private static void FunctiaSwitch(int nr)
    {
        String nrStr = null;
        switch (nr) {
            case 0:  nrStr = "ZERO";  break;
            case 1:  nrStr = "ONE";   break;
            case 2:  nrStr = "TWO";   break;
            case 3:  nrStr = "THREE"; break;
            case 4:  nrStr = "FOUR";  break;
            case 5:  nrStr = "FIVE";  break;
            case 6:  nrStr = "SEX";   break;
            case 7:  nrStr = "SEVEN"; break;
            case 8:  nrStr = "EIGHT"; break;
            case 9:  nrStr = "NINE";  break;
            default: nrStr = "OTHER"; break;
        }
        System.out.println("(b) Folosind functia Switch " + nrStr);
}
}
