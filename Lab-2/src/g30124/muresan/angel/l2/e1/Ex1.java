package g30124.muresan.angel.l2.e1;

import java.util.Scanner;

public class Ex1 {
	
	public static void main(String [] args)
	{   
		int a,b;
		Scanner in = new Scanner(System.in);
		
		System.out.println("Numarul a este egal cu: ");
		a = in.nextInt();
		
		System.out.println("Numarul b este egal cu: ");
		b = in.nextInt();
		
		in.close();
		if(a==b)
			System.out.println("Numerele sunt egale");
		else
		if(a>b)
			System.out.println("Numarul maxim este a= "+a);
		else
			System.out.println("Numarul maxim este b= "+b);
	}
	
}
