package g30124.muresan.angel.l2.e4;

import java.util.Scanner;

public class Ex4 {
	
    public static void main(String[] args) 

    {
        int n, max;
        Scanner s = new Scanner(System.in);
        System.out.print("Introduceti numarul de elemente a vectorului:");
        n = s.nextInt();
        int a[] = new int[n];
        System.out.println("Introduceti elementele vectorului:");
        for(int i = 0; i < n; i++)
        {
            a[i] = s.nextInt();
        }
        max = a[0];
        for(int i = 0; i < n; i++)
        {
            if(max < a[i])
            {
                max = a[i];
            }
        }
        System.out.println("Valoarea maxima din vector este:"+max);
    }
    
}
