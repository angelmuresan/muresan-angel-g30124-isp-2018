package g30124.muresan.angel.l2.e6;

import java.util.Scanner;

public class Ex6 {
	
	static int Nerecursiv(int nr){
		int i,f=1;
		for(i=1; i<=nr;i++)
			f=f*i;
		return f;
	}
	static int Recursiv(int nr){
		if (nr==1) return 1;
		else return nr*Recursiv(nr-1);
	}
	public static void main(String[] args){
		int nr;
		
		Scanner in = new Scanner(System.in);
		System.out.println("Dati n= ");
		nr = in.nextInt();
		in.close();
		
		System.out.println("Factorial nerecursiv: "+Nerecursiv(nr));
		System.out.println("Factorialul recursiv: "+Recursiv(nr));
	}
}
